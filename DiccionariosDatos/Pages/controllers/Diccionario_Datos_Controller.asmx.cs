﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DiccionariosDatos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Telerik.Reporting;

namespace DiccionariosDatos.Pages.controllers
{
    /// <summary>
    /// Descripción breve de Diccionario_Datos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Diccionario_Datos_Controller : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GuardarConexion(string jsonObject)
        {
            string jsonResultado = "";
            Cls_Mensaje _Mensaje = new Cls_Mensaje();
            Cls_Apl_Diccionario_Datos_Negocio obj = new Cls_Apl_Diccionario_Datos_Negocio();
            try
            {
                _Mensaje.Titulo = "Conexión";

                Cls_Sesiones.ServerName = null;
                Cls_Sesiones.UserBD = null;
                Cls_Sesiones.Password = null;
                Cls_Sesiones.Database = null;
                Cls_Sesiones.Str_Conexion = null;

                obj = JsonConvert.DeserializeObject<Cls_Apl_Diccionario_Datos_Negocio>(jsonObject);
                string connectionString = "Data Source=" + obj.ServerName + ";Initial Catalog=" + obj.DataBase + ";User ID=" + obj.User + ";Password=" + obj.Password;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Cls_Sesiones.ServerName = obj.ServerName;
                    Cls_Sesiones.UserBD = obj.User;
                    Cls_Sesiones.Password = obj.Password;
                    Cls_Sesiones.Database = obj.DataBase;
                    Cls_Sesiones.Str_Conexion = connectionString;
                }

                if (string.IsNullOrEmpty(Cls_Sesiones.Str_Conexion))
                {
                    _Mensaje.Estatus = false;
                    _Mensaje.Mensaje = "Error al hacer la conexión. Favor de verificar las credenciales.";
                }
                else
                {
                    _Mensaje.Estatus = true;
                    _Mensaje.Mensaje = "La conexión se establecio correctamente.";
                }
            }
            catch (SqlException ex) {
                _Mensaje.Estatus = false;
                _Mensaje.Mensaje = "Error al hacer la conexión. Favor de verificar las credenciales. " + ex.Message;

            }
            catch (Exception e)
            {
                _Mensaje.Estatus = false;
                _Mensaje.Mensaje = "Error al conectar. " + e.Message;
            }
            jsonResultado = JsonConvert.SerializeObject(_Mensaje);

            return jsonResultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Conexion()
        {
            string jsonResultado = "";
            Cls_Mensaje _Mensaje = new Cls_Mensaje();
            Cls_Apl_Diccionario_Datos_Negocio obj = new Cls_Apl_Diccionario_Datos_Negocio();
            try
            {
                _Mensaje.Titulo = "Conexión";
                if (!string.IsNullOrEmpty(Cls_Sesiones.Str_Conexion))
                {
                    obj.ServerName = Cls_Sesiones.ServerName;
                    obj.User = Cls_Sesiones.UserBD;
                    obj.Password = Cls_Sesiones.Password;
                    obj.DataBase = Cls_Sesiones.Database;

                    _Mensaje.Estatus = true;
                    _Mensaje.Mensaje = "Conexion";
                    _Mensaje.Items = obj;
                }
                else
                {
                    _Mensaje.Estatus = false;
                    _Mensaje.Mensaje = "Conexion no establecida";

                }
       
            }
            catch (Exception e)
            {
                _Mensaje.Estatus = false;
                _Mensaje.Mensaje = "Error al consultar conectar. " + e.Message;
            }
            jsonResultado = JsonConvert.SerializeObject(_Mensaje);

            return jsonResultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string CerrarConexion()
        {
            string jsonResultado = "";
            Cls_Mensaje _Mensaje = new Cls_Mensaje();
            Cls_Apl_Diccionario_Datos_Negocio obj = new Cls_Apl_Diccionario_Datos_Negocio();
            try
            {
                _Mensaje.Titulo = "Conexión";

                Cls_Sesiones.ServerName = null;
                Cls_Sesiones.UserBD = null;
                Cls_Sesiones.Password = null;
                Cls_Sesiones.Database = null;
                Cls_Sesiones.Str_Conexion = null;


                _Mensaje.Estatus = true;
                _Mensaje.Mensaje = "La conexión se cerró correctamente.";

            }
            catch (Exception e)
            {
                _Mensaje.Estatus = false;
                _Mensaje.Mensaje = "Error al conectar. " + e.Message;
            }
            jsonResultado = JsonConvert.SerializeObject(_Mensaje);

            return jsonResultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            string json_resultado = "";
            Cls_Mensaje mensaje = new Cls_Mensaje();
            List<Cls_Apl_Diccionario_Datos_Negocio> Lst = new List<Cls_Apl_Diccionario_Datos_Negocio>();
            try
            {

                mensaje.Titulo = "Alta/Actualizacion del diccionario de datos.";
                Lst = JsonConvert.DeserializeObject<List<Cls_Apl_Diccionario_Datos_Negocio>>(jsonObject);
                               
                foreach (var Filtros in Lst)
                {

                    if (string.IsNullOrEmpty(Filtros.Column_Name))
                    {
                        string sp = (Filtros.Estatus == "AGREGAR" ? "sys.sp_addextendedproperty" : "sys.sp_updateextendedproperty"); //+ " @name, @value, @level0type, @level0name, @level1type, @level1name";

                        var consulta = SqlHelper.ExecuteDataset(Cls_Sesiones.Str_Conexion, sp, Filtros.Name, Filtros.Value, Filtros.Schema, Filtros.Schema_Type, Filtros.Table, Filtros.Table_Name, null, null);

                    }
                    else
                    {
                        string sp = (Filtros.Estatus == "AGREGAR" ? "sys.sp_addextendedproperty" : "sys.sp_updateextendedproperty"); //+ " @name, @value, @level0type, @level0name, @level1type, @level1name, @level2type, @level2name";

                        var consulta = SqlHelper.ExecuteDataset(Cls_Sesiones.Str_Conexion, sp, Filtros.Name, Filtros.Value, Filtros.Schema, Filtros.Schema_Type, Filtros.Table, Filtros.Table_Name, Filtros.Column, Filtros.Column_Name); // Cls_Metodos_Generales.executeProc<object>(sp, Parametros);


                    }
                }
                mensaje.Estatus = true;
                mensaje.Mensaje = "La operacion se realizó correctamente.";
            }
            catch (Exception ex)
            {
                mensaje.Estatus = false;
                mensaje.Mensaje = "Error al guardar/actualizar el diccionario de datos. " + ex.Message;
            }

            json_resultado = JsonConvert.SerializeObject(mensaje);

            return json_resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Diccionario_Datos(string jsonObject)
        {
            string jsonresultado = "";

            try
            {
                Cls_Apl_Diccionario_Datos_Negocio Filtros = new Cls_Apl_Diccionario_Datos_Negocio();

                Filtros = JsonConvert.DeserializeObject<Cls_Apl_Diccionario_Datos_Negocio>(jsonObject);

       
                DataTable dt = Consulta_Diccionario(Filtros);

                //var consulta = Cls_Metodos_Generales.executeProc<Cls_Apl_Diccionario_Datos_Negocio>(sp, Parametros);
                jsonresultado = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                jsonresultado = "[]";

            }

            return jsonresultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Diccionario_Datos_Tablas(string jsonObject)
        {
            string jsonresultado = "";

            try
            {
                Cls_Apl_Diccionario_Datos_Negocio Filtros = new Cls_Apl_Diccionario_Datos_Negocio();

                Filtros = JsonConvert.DeserializeObject<Cls_Apl_Diccionario_Datos_Negocio>(jsonObject);

        

                DataTable consulta = Consulta_Diccionario_Tablas(Filtros);
                jsonresultado = JsonConvert.SerializeObject(consulta);
            }
            catch (Exception ex)
            {
                jsonresultado = "[]";

            }

            return jsonresultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Nombre_Tablas()
        {
            string Json_Resultado = string.Empty;
            DataTable Lst_Combo = new DataTable();
           
            try
            {
                string q = string.Empty;

                string estatus = string.Empty;

                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                if (!String.IsNullOrEmpty(nvc["estatus"]))
                    estatus = nvc["estatus"].ToString().Trim();

                Cls_Apl_Diccionario_Datos_Negocio Filtros = new Cls_Apl_Diccionario_Datos_Negocio();



                Lst_Combo = Consulta_Tablas_Combo(q, estatus);
                Json_Resultado = JsonConvert.SerializeObject(Lst_Combo);

            }
            catch (Exception Ex)
            {
                Json_Resultado = "[]";

            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Exportar(string jsonObject)
        {

            Cls_Apl_Diccionario_Datos_Negocio Obj_Filtros = new Cls_Apl_Diccionario_Datos_Negocio();
            string jsonResultado = "";
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {

                Obj_Filtros = JsonConvert.DeserializeObject<Cls_Apl_Diccionario_Datos_Negocio>(jsonObject);


                List<Telerik_Reporting_Parameters> lst = new List<Telerik_Reporting_Parameters>();

                lst.Add(new Telerik_Reporting_Parameters() { Nombre_Parametro = "TableName", Valor_Parametro = Obj_Filtros.Table_Name });


                String reportName = Server.MapPath("~/Reports/Rpt_Diccionario_Datos.trdp");
                String reportPath = Server.MapPath("~/Reportes/");
                string Formato = string.IsNullOrEmpty(Obj_Filtros.formatReport) ? "PDF" : Obj_Filtros.formatReport;

                string Filename = Generar_Telerik_Reporting(reportName, lst, Formato, reportPath);

                if (!string.IsNullOrEmpty(Filename))
                {
                    Mensaje.Archivo_Url = "../../Reportes/" + Filename;
                    Mensaje.Estatus = true;
                    Mensaje.Mensaje = "Se genero el archivo correctamente.";
                }
                else
                {
                    Mensaje.Estatus = false;
                    Mensaje.Mensaje = "Error al generar el documento. ";

                }



            }
            catch (Exception e)
            {
                Mensaje.Estatus = false;
                Mensaje.Mensaje = "Error al generar el documento. " + e.Message;
            }
            finally
            {
                jsonResultado = JsonConvert.SerializeObject(Mensaje);
            }
            return jsonResultado;
        }

        public string Generar_Telerik_Reporting(string reportName, List<Telerik_Reporting_Parameters> parameters, string format, string path)
        {

            string nombreArchivo = "";
            string[] formats = { "MHTML", "PDF", "XLS", "XLSX", "RTF", "DOCX", "PPTX", "CSV" };

            try
            {
                var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                var reportSource = new UriReportSource();
                var deviceInfo = new System.Collections.Hashtable();

                reportSource.Uri = reportName;

                if (parameters != null)
                {
                    foreach (var item_parameter in parameters)
                    {
                        reportSource.Parameters.Add(item_parameter.Nombre_Parametro, item_parameter.Valor_Parametro);
                    }

                }
                Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport(format, reportSource, deviceInfo);

                string fileName = result.DocumentName + "." + result.Extension;
                //string path = System.IO.Path.GetTempPath();

                string filePath = System.IO.Path.Combine(path, fileName);

                using (System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
                {
                    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                }
                nombreArchivo = fileName;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            return nombreArchivo;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Generar_Nota_Devolucion(string jsonObject)
        {
            string JsonResultado = "";
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            Cls_Apl_Diccionario_Datos_Negocio Negocio = new Cls_Apl_Diccionario_Datos_Negocio();
            DataTable dt = new DataTable();
            try
            {
                Mensaje.Titulo = "Generar Nota de Devolucion";
                Negocio = JsonConvert.DeserializeObject<Cls_Apl_Diccionario_Datos_Negocio>(jsonObject);



                string Ruta_Archivo = "";

                if (Negocio == null)
                    Negocio = new Cls_Apl_Diccionario_Datos_Negocio();

                dt = Consulta_Diccionario(Negocio);

                Ruta_Archivo = Generar_Nota_Devolucion_PDF(dt);

                Mensaje.Estatus = !string.IsNullOrEmpty(Ruta_Archivo);
                Mensaje.Mensaje = string.IsNullOrEmpty(Ruta_Archivo) ? "Error al generar el reporte." : "Nota de devolucion generado.";
                Mensaje.Archivo_Url = string.IsNullOrEmpty(Ruta_Archivo) ? "" : Ruta_Archivo;

            }
            catch (Exception ex)
            {
                Mensaje.Estatus = false;
                Mensaje.Mensaje = "Error al generar el reporte.";
                Mensaje.Archivo_Url = "";
            }
            finally
            {
                JsonResultado = JsonConvert.SerializeObject(Mensaje);
            }

            return JsonResultado;
        }


        public string Generar_Nota_Devolucion_PDF(DataTable obj)
        {

            string Generado = "";
            DataSet Ds_Datos = new DataSet();
            DataTable Dt_Devolucion = new DataTable();



            Dt_Devolucion = obj;
            Dt_Devolucion.TableName = "Dt_Diccionario_Datos";
            Ds_Datos.Tables.Add(Dt_Devolucion.Copy());
            

            string Nombre = "Diccionario de datos.pdf";

            string ruta = Generar_Reporte(ref Ds_Datos, "CR_Diccionario_Datos.rpt", Nombre);

            return ruta;
        }


        protected string Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
        {
            string _reponse = string.Empty;
            ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
            ReportDocument SubReporte = new ReportDocument();
            ReportDocument SubR_Percepciones = new ReportDocument();
            String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 
            //String Ruta_Subreporte_Deducciones = String.Empty;
            //String Ruta_Subreporte_Percepcuiones = String.Empty;

            try
            {
                Ruta = System.Web.Hosting.HostingEnvironment.MapPath(@"~/" + "Reports/" + Nombre_Plantilla_Reporte);

                Reporte.Load(Ruta);

                if (Ds_Datos is DataSet)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Datos);
                        //Reporte.SetParameterValue("Titulo_Reporte", "Reporte General");
                        bool generado = Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        _reponse = generado ? "../Reports/" + Nombre_Reporte_Generar : "";
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }

            return _reponse;
        }

        private bool Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
        {
            bool Generado = false;
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = System.Web.Hosting.HostingEnvironment.MapPath(@"~/" + "Reports/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                    Generado = true;
                }
            }
            catch (Exception Ex)
            {
                Generado = false;
                //throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
            return Generado;
        }

        public DataTable Consulta_Diccionario(Cls_Apl_Diccionario_Datos_Negocio apl)
        {
            String _query;//variable para las consultas
            DataTable Dt_Aux;//Tabla axuliar paras las consultas


            try
            {

                string str = "Data Source=" + Cls_Sesiones.ServerName + ";User ID=" + Cls_Sesiones.UserBD + ";Password=" + Cls_Sesiones.Password;

                string s = "SELECT name from sys.databases";
                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(str, CommandType.Text, s).Tables[0];

                _query = "SELECT" +
                "   CAST(ROW_NUMBER() OVER(ORDER BY tab.[Table_Name] ASC) AS INT) - 1 AS id" +
                ",* " +
                " FROM(SELECT DISTINCT " +
                "   ISNULL(exprop.name, 'MS_' + UPPER(clmns.name)) AS 'Name' " +
                ", CAST(exprop.value AS VARCHAR(MAX)) 'Value' " +
                  ", 'SCHEMA' AS 'Schema' " +
                  ", 'dbo' AS Schema_Type " +
                  ", 'TABLE' 'Table' " +
                  ", tbl.name Table_Name " +
                  ", 'COLUMN' AS 'Column' " +
                  ", CAST(clmns.name AS VARCHAR(35)) Column_Name " +
                  ", CASE " +
                   "    WHEN " +
                    "       exprop.name IS NULL THEN 'NUEVO' " +
                     "  ELSE 'EXISTENTE' " +
                  " END Estatus " +
                  ", clmns.is_nullable Is_Null " +
                  ", clmns.is_identity Primary_Key " +
                  ", CASE " +
                   "    WHEN fkclmn.parent_column_id IS NOT NULL THEN CAST(1 AS BIT) " +
                    "   ELSE CAST(0 AS BIT) " +
                   " END Foreign_Key " +
                  ", udt.name Tipo_Dato " +
                  ", exproptbl.value Value_Table " +
                  ", clmns.max_length " +
                  ", clmns.column_id " +
                " FROM sys.tables AS tbl " +
                " LEFT JOIN sys.columns AS clmns " +
                 "  ON clmns.object_id = tbl.object_id " +
                " LEFT JOIN sys.foreign_key_columns fkclmn " +
                 "  ON fkclmn.parent_column_id = clmns.column_id " +
                  " AND fkclmn.parent_object_id = clmns.object_id " +
                " LEFT JOIN sys.extended_properties exprop " +
                 "  ON exprop.major_id = clmns.object_id " +
                  " AND exprop.minor_id = clmns.column_id " +
                "   LEFT OUTER JOIN sys.types AS udt " +
                "      ON udt.user_type_id = clmns.user_type_id " +
                " LEFT JOIN sys.extended_properties exproptbl " +
                "    ON exproptbl.major_id = tbl.object_id " +
                "   AND exproptbl.minor_id = 0 ";

                if (!string.IsNullOrEmpty(apl.Table_Name))
                    _query += " where tbl.name = '" + apl.Table_Name + "'";

               _query +=  ") AS tab " +
                " ORDER BY tab.[Table_Name], tab.column_id ASC";

                

                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Sesiones.Str_Conexion, CommandType.Text, _query).Tables[0];

              

           
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Dt_Aux;
        }

        public DataTable Consulta_Diccionario_Tablas(Cls_Apl_Diccionario_Datos_Negocio apl)
        {
            String _query;//variable para las consultas
            DataTable Dt_Aux;//Tabla axuliar paras las consultas


            try
            {

                _query = "SELECT " +

                    "CAST(ROW_NUMBER() OVER(ORDER BY tbl.name ASC) AS INT) - 1 AS id " +
                  ", ISNULL(exproptbl.name, 'TB_' + UPPER(tbl.name)) AS 'Name' " +
                   ",CAST(exproptbl.value AS VARCHAR(MAX)) 'Value' " +
                   ",'SCHEMA' AS 'Schema' " +
                   ",'dbo' AS Schema_Type " +
                   ",'TABLE' 'Table' " +
                   ",tbl.name Table_Name " +
                   ", NULL AS 'Column' " +
                   ",NULL Column_Name " +
                   ", CASE " +
                    "    WHEN " +
                     "       exproptbl.name IS NULL THEN 'NUEVO' " +
                      "  ELSE 'EXISTENTE' " +
                    " END Estatus " +
                " FROM sys.tables AS tbl " +
                " LEFT JOIN sys.extended_properties exproptbl " +
                  "  ON exproptbl.major_id = tbl.object_id " +
                   "     AND exproptbl.minor_id = 0 ";


                if (!string.IsNullOrEmpty(apl.Table_Name))
                    _query += " where tbl.name = '" + apl.Table_Name + "'";

                _query += " ORDER BY tbl.name ASC";



                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Sesiones.Str_Conexion, CommandType.Text, _query).Tables[0];




            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Dt_Aux;
        }

        public DataTable Consulta_Tablas_Combo(string q, string estatus)
        {
            String _query;//variable para las consultas
            DataTable Dt_Aux;//Tabla axuliar paras las consultas
           

            bool _where = false;
            try
            {

                    _query = "SELECT " +
                    " tbl.name AS id " +
                    " ,tbl.name AS[text] " +
                    " , tbl.Estatus AS[detalle_1] " +
                                          " FROM(SELECT " +
                      "       tabla.name " +
                        "    , CASE " +
                          "       WHEN tabla.NumColsPend = 0 THEN 'COMPLETO' " +
                            "     ELSE CASE " +
                              "           WHEN tabla.NumCols = tabla.NumColsPend THEN 'SIN INICIAR' " +
                                "         ELSE 'PENDIENTE' " +
                                  "   END " +
                            " END Estatus " +
                        " FROM(SELECT " +
                        "       (SELECT " +
                            "             COUNT(*) " +
                              "       FROM sys.columns c " +
                                "     WHERE c.object_id = t.object_id) " +
                                " NumCols " +
                                " , (SELECT " +
                                 "        COUNT(*) " +
                                   "  FROM sys.columns c " +
                                    " LEFT JOIN sys.extended_properties exprop " +
                                      "   ON exprop.major_id = c.object_id " +
                                        " AND exprop.minor_id = c.column_id " +
                                    " WHERE c.object_id = t.object_id " +
                                    " AND exprop.value IS NULL) " +
                                " NumColsPend " +
                               " , t.name " +
                            " FROM sys.tables t) AS tabla) AS tbl ";


                if (!string.IsNullOrEmpty(q))
                {
                    _query += " where tbl.name like'%" + q + "%'";
                    _where = true;
                }

                if (!string.IsNullOrEmpty(estatus))
                {
                    if (_where)
                        _query += " and tbl.Estatus = '" + estatus + "'";
                    else
                        _query += " where tbl.Estatus = '" + estatus + "'";

                }

                _query += " ORDER BY tbl.name";



                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Sesiones.Str_Conexion, CommandType.Text, _query).Tables[0];




            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Dt_Aux;
        }

   

    }
}
