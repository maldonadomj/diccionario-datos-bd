﻿var editor;

$(document).on('ready', function () {
    _crear_tbl_diccionario_datos();
    _verificar_conexion();
  
    _load_cmb_tablas_filtros();
    
    _inicializar_icheck();
    _eventos();

});

function _habilitar_controles(option) {
    try {        

        switch (option) {
            case 'Conectado':
                $('#toolbar button[type=button]').each(function () { $(this).attr('disabled', false); });

                $('#rd_columna, #rd_tabla').iCheck('enable');
                $('#cmb_estatus').attr('disabled', false);
                $('#cmb_tablas_filtros').attr('disabled', false);


                $('#btn_connect').attr('title', 'Desconectar');
                $('#btn_connect').removeClass().addClass('btn btn-danger');
                $('#btn_connect i').removeClass().addClass('fas fa-times');
                $('#btn_connect svg').removeClass('fa-sign-in-alt').addClass('fa-times');
                _limpiar_controles();
                break;
            case 'Desconectado':
                $('#rd_columna, #rd_tabla').iCheck('disable');
                $('#cmb_estatus').attr('disabled', true);
                $('#cmb_tablas_filtros').attr('disabled', true);
                $('#toolbar button[type=button]').each(function () { $(this).attr('disabled', true); });
                $('#btn_connect').attr('disabled', false);
                $('#btn_connect').attr('title', 'Establecer conexion');
                $('#btn_connect').removeClass().addClass('btn btn-info');
                $('#btn_connect i').removeClass().addClass('fas fa-sign-in-alt');
                $('#btn_connect svg').removeClass('fa-times').addClass('fa-sign-in-alt');
                _limpiar_controles();
                break;
        }
               
    } catch (e) {
        _showMessageDanger('Error Tecnico', e);
    }

}

function _limpiar_controles() {
    $('#rd_columna').iCheck('check');
    $('#cmb_estatus').val('');
    $('#cmb_tablas_filtros').empty().trigger('change');
    $('#tbl_diccionario_datos').DataTable().clear().draw();
}

function _eventos() {
    $('#btn_connect').on('click', function (e) {
        e.preventDefault();
        var title = $(this).attr('title');
        if (title == "Desconectar") {
            bootbox.confirm({
                title: 'Desconectar',
                message: '¿Esta seguro de desconectar la conexión?',
                callback: function (result) {
                    if (result) {
                        _cerrar_conexion();
                    }
                }
            });
       
        } else {
            _validation_sumary(null);
            $('#txt_nombre_servidor').val('');
            $('#txt_usuario').val('');
            $('#txt_password').val('');
            $('#txt_base_datos').val('');
            $('#btn_mostrar_password').prop('title', 'Mostrar');
            $('#txt_password').prop('type', 'password');
            $('#basic-addon2 svg').removeClass('fa-eye-slash').addClass('fa-eye');
            $('#modal_connect_db').modal('show');
        }
    });

    $('#btn_busqueda').on('click', function (e) {
        e.preventDefault();
        _consultar();
    });

    $('#btn_guardar').on('click', function (e) {
        e.preventDefault();
        _alta_actualizar();
    });

    $('#tbl_diccionario_datos').DataTable().on('key-focus', function (e, datatable, cell, originalEvent) {

        editor.off('edit');
        editor.on('edit', function (e, json, datos, id) {



            if (datos.Estatus == "NUEVO") {
                datos.Estatus = "AGREGAR";
            } else if (datos.Estatus == "EXISTENTE") {
                datos.Estatus = "EDITAR";
            }

            $('#tbl_diccionario_datos').DataTable().row(id).data(datos);



        });
    });
    
    $('input.icheck-11').on('ifChanged', function (e) {
        if ($('#cmb_tablas_filtros').select2('data').length > 0)
            _consultar();
        else
            $('#tbl_diccionario_datos').DataTable().clear().draw();
    });

    $('#cmb_estatus').on('change', function (e) {
        e.preventDefault();
        $('#cmb_tablas_filtros').empty().trigger('change');
        $('#tbl_diccionario_datos').DataTable().clear().draw();
    });

    $('#btn_conexion').on('click', function (e) {
        e.preventDefault();

        var validacion = _validacion();
        if (validacion.Estatus) {

            $('#modal_connect_db button[type=button]').each(function () { $(this).attr('disabled', true); });
            $('#btn_conexion svg').removeAttr('fa-sign-in-alt').addClass('fa-spinner fa-spin');
            _crearConexion();
        }
    });

    $('input').each(function (index, element) {
        $(this).on('focus', function () {
            _remove_class_error('#' + $(this).attr('id'));
        });
    });

    $('#btn_mostrar_password').on('click', function (e) {
        e.preventDefault();
        var title = $(this).attr('title');
        if (title == "Mostrar") {
            $(this).prop('title', 'Ocultar');
            $('#txt_password').prop('type', 'text');
            $('#basic-addon2 svg').removeClass('fa-eye').addClass('fa-eye-slash');
        } else {
            $(this).prop('title', 'Mostrar');
            $('#txt_password').prop('type', 'password');
            $('#basic-addon2 svg').removeClass('fa-eye-slash').addClass('fa-eye');
        }

    });
}

function _crear_tbl_diccionario_datos() {

    editor = new $.fn.dataTable.Editor({
        data: [],
        table: "#tbl_diccionario_datos",
        idSrc: 'id',
        fields: [
            {
                label: "Value:",
                name: "Value",
            }]
    });


    $('#tbl_diccionario_datos').DataTable({
        retrieve: true,
        autoWidth: true,
        //scrollY: '500px',
        //scrollCollapse: true,
        order: [[0, 'asc']],
        language: {
            url: '../Resources/DataTable/Es-Mx.json'
        },
        columns: [
            { data: 'id', title: '#', visible: false },
            { data: 'Table_Name', title: 'Tabla', orderable: false },
            { data: 'Column_Name', title: 'Columna', orderable: false, },
            { data: 'Name', title: 'Nombre Diccionario', orderable: false, visible: false },
            { data: 'Value', title: 'Descripcion', orderable: false, className: 'text-blue text-bold', width:'700px' },
            //{ data: 'Schema', title: 'Schema', orderable: false },
            //{ data: 'Schema_Type', title: 'Schema_Type', orderable: false },
            //{ data: 'Table', title: 'Table', orderable: false },

            //{ data: 'Column', title: 'Column', orderable: false },

            {
                data: 'Estatus', title: '', orderable: false, render: function (data, type, row) {
                    var opt = "";

                    if (data == 'NUEVO')
                        opt = '<i class="fas fa-plus text-info" title="Nuevo"></i>';

                    if (data == 'EXISTENTE')
                        opt = '<i class="fas fa-check-circle text-info" title="Agregado"></i>';

                    if (data == 'AGREGAR')
                        opt = '<i class="fas fa-check text-success" title="Agregar"></i>';

                    if (data == 'EDITAR')
                        opt = '<i class="fas fa-edit text-success" title="Actualizar"></i>';

                    return opt;

                }
            },
        ],
        keys: {
            columns: [4],
            keys: [9, 13],
            editor: editor,
            editOnFocus: true
        },
    });

}

function _consultar() {
    try {

        var obj = new Object();

        var url = 'controllers/Diccionario_Datos_Controller.asmx/Consultar_Diccionario_Datos';

        var rd = $('#rd_tabla').prop('checked');
        if (rd) {
            url = 'controllers/Diccionario_Datos_Controller.asmx/Consultar_Diccionario_Datos_Tablas';
        }

        obj.Table_Name = $('#cmb_tablas_filtros').val();



        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(obj) });

        $.ajax({
            type: 'POST',
            url: url,
            data: $data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var resultado = JSON.parse(datos.d);
                    $('#tbl_diccionario_datos').DataTable().clear().draw();
                    $('#tbl_diccionario_datos').DataTable().rows.add(resultado).draw();

                }
            }
        });

    } catch (e) {
        _showMessageDanger('Error Tecnico', e);
    }

}

function _crearConexion() {
    try {

        var obj = new Object();

        obj.ServerName = $('#txt_nombre_servidor').val();
        obj.User = $('#txt_usuario').val();
        obj.Password = $('#txt_password').val();
        obj.DataBase = $('#txt_base_datos').val();



        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(obj) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Diccionario_Datos_Controller.asmx/GuardarConexion',
            data: $data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var resultado = JSON.parse(datos.d);
                    if (resultado.Estatus) {
                        _showMessageSuccess(resultado.Titulo, resultado.Mensaje);
                        $('#modal_connect_db').modal('hide');
                        _verificar_conexion();
                    } else {
                        _validation_sumary(resultado);
                    }

                }
                $('#modal_connect_db button[type=button]').each(function () { $(this).attr('disabled', false); });
                $('#btn_conexion svg').removeAttr('fa-spinner').removeClass('fa-spin').addClass('fa-sign-in-alt');
            }
        });

    } catch (e) {
        _showMessageDanger('Error Tecnico', e);
    }

}

function _load_cmb_tablas_filtros() {
    try {

        $('#cmb_tablas_filtros').select2({
            language: "es",
            theme: "classic",
            placeholder: 'SELECCIONE',
            allowClear: true,

            ajax: {
                url: 'controllers/Diccionario_Datos_Controller.asmx/Consultar_Nombre_Tablas',
                cache: true,
                dataType: 'json',
                type: "POST",
                delay: 250,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                        estatus: $('#cmb_estatus').val()
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }, templateResult: _formato,
            //templateSelection: _templateSelection
        });

        $('#cmb_tablas_filtros').on("select2:select", function (evt) {

            _consultar();
        });

        $('#cmb_tablas_filtros').on("select2:unselect", function (evt) {

            $('#tbl_diccionario_datos').DataTable().clear().draw();
        });

    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    }
}

function _formato(row) {
    if (!row.id) {
        return row.text;
    } else {
        var _salida = '<div class="row">' +
            '<div class="col-md-9">' +
            '<span style="">' +
            row.text +
            '<br />' +
            '<b style="font-size: x-small;">&nbsp;Estatus ' + row.detalle_1 + '</b>' +
            '</span>' +
            '</div>' +
            '</div>';

        return $(_salida);
    }
}

function _templateSelection(row) {
    //if (!row.id) { return row.text; }
    //else if (row.id === row.text) return row.text;

    var _salida = '<span>' +
        row.text + '&nbsp;Estatus ' + row.detalle_1 +
        '</span>';

    return $(_salida);
}

function _alta_actualizar() {
    try {

        var lst = [];

        var table = $('#tbl_diccionario_datos').DataTable().rows().data();

        for (var i = 0; i < table.length; i++) {
            var x = table[i];
            if (x.Estatus == "AGREGAR" || x.Estatus == "EDITAR")
                lst.push(x);
        }

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(lst) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Diccionario_Datos_Controller.asmx/Alta',
            data: $data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var data = JSON.parse(datos.d);
                    if (data.Estatus) {
                        _showMessageSuccess(data.Titulo, data.Mensaje);
                        _consultar();
                    }
                    else
                        _showMessageDanger(data.Titulo, data.Mensaje);
                } else {
                    _showMessageWarning('Error Tecnico', 'Error al obtener información del servidor');
                }
            }
        });

    } catch (e) {
        _showMessageDanger('Error Tecnico', e);
    }

}

function _verificar_conexion() {
    try {
        $.ajax({
            type: 'POST',
            url: 'controllers/Diccionario_Datos_Controller.asmx/Consultar_Conexion',
            //data: $data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var data = JSON.parse(datos.d);
                    if (data.Estatus) {
                        _habilitar_controles('Conectado');
                        //_showMessageSuccess(data.Titulo, data.Mensaje);
                        $('#_conexionBD').html('Servidor: ' + data.Items.ServerName + '; BD: ' + data.Items.DataBase);
                    }
                    else {
                        _habilitar_controles('Desconectado');
                        //_showMessageDanger(data.Titulo, data.Mensaje);
                        $('#_conexionBD').html('Sin Conexion');
                    }
                } else {
                    _showMessageWarning('Error Tecnico', 'Error al obtener información del servidor');
                }
            }
        });
    } catch (e) {

    }
}

function _cerrar_conexion() {
    try {
        $.ajax({
            type: 'POST',
            url: 'controllers/Diccionario_Datos_Controller.asmx/CerrarConexion',
            //data: $data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var data = JSON.parse(datos.d);
                    if (data.Estatus) {
                        _showMessageSuccess(data.Titulo, data.Mensaje);
                        $('#_conexionBD').html('Sin Conexion');
                        _habilitar_controles('Desconectado')
                    }
                    else {
                        _showMessageDanger(data.Titulo, data.Mensaje);
                        
                    }
                } else {
                    _showMessageWarning('Error Tecnico', 'Error al obtener información del servidor');
                }
            }
        });
    } catch (e) {

    }
}

function _exportar(data) {
    var obj = new Object();

    //var $tipo = $(data).data('tipo');
    var $format = $(data).data('formato');

    //if ($tipo == "global") {
    //    obj.Asignacion = $('#txt_asignacion_filtro').val();

    //} else {

    //    var $datos = $(data).data('datos');

    //    obj.No_Asignacion = $datos.No_Asignacion;
    //}
    obj.Table_Name = $('#cmb_tablas_filtros').val();
    obj.formatReport = $format;


    var $data = JSON.stringify({ 'jsonObject': JSON.stringify(obj) });

    $.ajax({
        type: 'POST',
        url: 'controllers/Diccionario_Datos_Controller.asmx/Generar_Nota_Devolucion',
        data: $data,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (datos) {
            if (datos !== null) {
                var resultado = JSON.parse(datos.d);
                if (resultado.Estatus) {
                    window.open(resultado.Archivo_Url, "", "", true);
                } else {
                    _showMessageDanger('Traspaso', resultado.Mensaje);
                }
            }
        }
    });
}

function _inicializar_icheck() {
    $('input.icheck-11').iCheck('destroy');
    $('input.icheck-11').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
}

function _showMessageSuccess(title, message) {
    bootbox.dialog({
        message: '<i class="fa fa-check text-success" style="font-size:50px !important; position: absolute;padding-top: 0px;padding-left: 10px;"></i>' +
            '<div style="padding-left: 85px;min-height:50px;">' + message + '</div>',
        title: title == null ? " " : title,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Cerrar',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}

function _showMessageDanger(title, message) {
    bootbox.dialog({
        message: '<i class="fa fa-times text-danger" style="font-size:50px !important; position: absolute;padding-top: 0px;padding-left: 10px;"></i>' +
            '<div style="padding-left: 85px;min-height:50px;text-overflow: ellipsis;overflow: hidden;">' + message + '</div>',
        title: title == null ? " " : title,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Cerrar',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}

function _showMessageWarning(title, message) {
    bootbox.dialog({
        message: '<i class="fa fa-exclamation-triangle text-warning" style="font-size:50px !important; position: absolute;padding-top: 0px;padding-left: 10px;"></i>' +
            '<div style="padding-left: 85px;min-height:50px;text-overflow: ellipsis;overflow: hidden;"">' + message + '</div>',
        title: title == null ? " " : title,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Cerrar',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}

function _validacion() {
    var _output = new Object();
    _validation_sumary(null);
    _output.Estatus = true;
    _output.Mensaje = '';

    if (!$('#txt_nombre_servidor').parsley().isValid()) {
        _add_class_error('#txt_nombre_servidor');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;La clave es un dato requerido.<br />';
    }
    if (!$('#txt_usuario').parsley().isValid()) {
        _add_class_error('#txt_usuario');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El nombre es un dato requerido.<br />';
    }

    if (!$('#txt_password').parsley().isValid()) {
        _add_class_error('#txt_password');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El email es un dato requerido.<br />';
    }
    if (!$('#txt_base_datos').parsley().isValid()) {
        _add_class_error('#txt_base_datos');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El email es un dato requerido.<br />';
    }
    if (!_output.Estatus) {
        _output.Mensaje = "Favor de llenar todos los datos.";
        _validation_sumary(_output);
    }

    return _output;
}

function _add_class_error(selector) {
    $(selector).addClass('alert-danger');
}

function _remove_class_error(selector) {
    $(selector).removeClass('alert-danger');
}

function _clear_all_class_error() {
    $('input').each(function (index, element) {
        _remove_class_error('#' + $(this).attr('id'));
    });
}

function _validation_sumary(validation) {
    var header_message = '<i class="fa fa-exclamation-triangle text-warning"></i><span>Observaciones</span><br />';

    if (validation == null) {
        $('#lbl_msg_error').html('');
        $('#sumary_error').css('display', 'none');
    } else {
        $('#lbl_msg_error').html(header_message + validation.Mensaje);
        $('#sumary_error').css('display', 'block');
    }
}