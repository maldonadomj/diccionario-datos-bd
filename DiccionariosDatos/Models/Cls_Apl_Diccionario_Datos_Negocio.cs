﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiccionariosDatos.Models
{
    public class Cls_Apl_Diccionario_Datos_Negocio
    {
        public int? id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Schema { get; set; }
        public string Schema_Type { get; set; }
        public string Table { get; set; }
        public string Table_Name { get; set; }
        public string Column { get; set; }
        public string Column_Name { get; set; }
        public string Estatus { get; set; }
        public string formatReport { get; set; }
        public string ServerName { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string DataBase { get; set; }
    }
}