﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiccionariosDatos.Models
{
    public class Cls_Mensaje
    {
        public string Titulo { get; set; }
        public bool Estatus { get; set; }
        public string Mensaje { get; set; }
        public object Items { get; set; }
        public string Archivo_Url { get; set; }
    }
}