﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiccionariosDatos.Models
{
    public class Telerik_Reporting_Parameters
    {
        public string Nombre_Parametro { get; set; }
        public object Valor_Parametro { get; set; }
    }
}