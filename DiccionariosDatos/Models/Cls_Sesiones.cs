﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiccionariosDatos.Models
{
    public class Cls_Sesiones
    {
        private static string s_ServerName = "ServerName";
        private static string s_UserBD = "UserBD";
        private static string s_Password = "Password";
        private static string s_Database = "Database";
        private static string s_CadenaConexion = "CadenaConexion";

        public static string ServerName
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sesiones.s_ServerName] == null)
                    return null;
                else
                    return (string)HttpContext.Current.Session[Cls_Sesiones.s_ServerName];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sesiones.s_ServerName] = value;
            }
        }

        public static string UserBD
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sesiones.s_UserBD] == null)
                    return null;
                else
                    return (string)HttpContext.Current.Session[Cls_Sesiones.s_UserBD];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sesiones.s_UserBD] = value;
            }
        }

        public static string Password
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sesiones.s_Password] == null)
                    return null;
                else
                    return (string)HttpContext.Current.Session[Cls_Sesiones.s_Password];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sesiones.s_Password] = value;
            }
        }

        public static string Database
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sesiones.s_Database] == null)
                    return null;
                else
                    return (string)HttpContext.Current.Session[Cls_Sesiones.s_Database];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sesiones.s_Database] = value;
            }
        }


        public static string Str_Conexion
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sesiones.s_CadenaConexion] == null)
                    return null;
                else
                    return (string)HttpContext.Current.Session[Cls_Sesiones.s_CadenaConexion];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sesiones.s_CadenaConexion] = value;
            }
        }
    }
}